const chai = require("chai");
const { expect } = require("chai");

const http = require("chai-http");
chai.use(http);

describe("api_test_suite", () => {
  it("test_api_get_people_is_running", () => {
    chai
      .request("http://localhost:5001")
      .get("/people")
      .end((err, res) => {
        expect(res).to.not.equal(undefined);
      });
  });

  it("test_api_get_people_returns_200", (done) => {
    chai
      .request("http://localhost:5001")
      .get("/people")
      .end((err, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  });

  it("test_api_post_person_returns_400_if_no_person_name", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/person")
      .type("json")
      .send({
        alias: "james",
        age: 28,
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("test_api_post_person_returns_400_if_age_is_string", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/person")
      .type("json")
      .send({
        name: "james",
        age: "Twenty-Eight",
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });
});

//Activity
describe("api_test_suite_activity", () => {
  it("test_api_post_person_is_running", () => {
    chai
      .request("http://localhost:5001")
      .get("/person")
      .end((err, res) => {
        expect(res).to.not.equal(undefined);
      });
  });

  it("test_api_post_person_returns_400_if_no_ALIAS", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/person")
      .type("json")
      .send({
        fullName: "james taylor Jr.",
        age: 28,
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("test_api_post_person_returns_400_if_no_AGE", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/person")
      .type("json")
      .send({
        name: "james",
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });
});
