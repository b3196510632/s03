const names = {
  brandon: {
    name: "Brandon boyd",
    age: 35,
  },
  Steve: {
    name: "Style Tyler",
    age: 56,
  },
};

module.exports = {
  names: names,
};
